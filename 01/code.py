import math

def get_input():
    lines = ""
    with open('./01/input.txt') as f:
        lines = f.readlines()
    return lines

if __name__ == "__main__":
    lines = get_input()
    for line1 in lines:
        for line2 in lines:
            for line3 in lines:
                if (int(line1) + int(line2) + int(line3) == 2020):
                    print(int(line1) * int(line2) * int(line3))

